export declare const env: {
    project: {
        name: string;
        description: string;
        email: string;
        port: number;
    };
    api: {
        morgan: string;
        pagination: {};
    };
    cors: {
        origin: boolean;
        methods: string[];
        allowedHeaders: string[];
        exposedHeaders: string[];
    };
    mongodb: {
        uri: string;
        uriTest: string;
        reset: boolean;
    };
};
