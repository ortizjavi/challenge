"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.env = void 0;
exports.env = {
    project: {
        name: 'App API',
        description: 'App API',
        email: 'hi@app.es',
        port: 3000
    },
    api: {
        morgan: 'dev',
        pagination: {}
    },
    cors: {
        origin: true,
        methods: ['GET', 'PATCH', 'POST', 'DELETE'],
        allowedHeaders: ['Content-Type', 'Authorization', 'Content-Length'],
        exposedHeaders: ['x-provider', 'limit', 'page', 'count', 'X-Response-Time']
    },
    mongodb: {
        uri: 'mongodb://database:27017/reign',
        uriTest: 'mongodb://database:27017/reign-test',
        reset: true,
    },
};
//# sourceMappingURL=environments.js.map