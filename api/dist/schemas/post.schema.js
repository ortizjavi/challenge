"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PostSchema = void 0;
const mongoose = require("mongoose");
exports.PostSchema = new mongoose.Schema({
    title: String,
    url: String,
    created_at_i: Number,
    author: String
}, { timestamps: false });
//# sourceMappingURL=post.schema.js.map