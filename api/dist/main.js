"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const compression = require("compression");
const cors = require("cors");
const helmet = require("helmet");
const morgan = require("morgan");
const debug = require("debug");
const environments_1 = require("./environments/environments");
const app_module_1 = require("./app.module");
const log = debug('app:server');
async function bootstrap() {
    const app = await core_1.NestFactory.create(app_module_1.AppModule);
    app.use(compression());
    app.use(morgan(environments_1.env.api.morgan));
    app.use(cors(environments_1.env.cors));
    app.use(helmet.hidePoweredBy());
    app.use(helmet.frameguard({ action: 'sameorigin' }));
    app.use(helmet.xssFilter());
    await app.listen(environments_1.env.project.port);
}
bootstrap();
//# sourceMappingURL=main.js.map