"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var CronService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.CronService = void 0;
const mongoose_1 = require("mongoose");
const common_1 = require("@nestjs/common");
const schedule_1 = require("@nestjs/schedule");
const post_service_1 = require("../post/post.service");
let CronService = CronService_1 = class CronService {
    constructor(cronPostsModel, postService) {
        this.cronPostsModel = cronPostsModel;
        this.postService = postService;
        this.logger = new common_1.Logger(CronService_1.name);
        this.lastUpdatePosts = null;
    }
    runEveryHour() {
        this.logger.debug('Called when the current second is 45');
        const post = { title: undefined, url: 'Mundo' };
    }
};
__decorate([
    (0, schedule_1.Cron)(schedule_1.CronExpression.EVERY_HOUR),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], CronService.prototype, "runEveryHour", null);
CronService = CronService_1 = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)('CronPosts')),
    __metadata("design:paramtypes", [mongoose_1.Model,
        post_service_1.PostService])
], CronService);
exports.CronService = CronService;
//# sourceMappingURL=cron.service.js.map