"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.cronPostsProviders = void 0;
const cronPosts_schema_1 = require("./schemas/cronPosts.schema");
exports.cronPostsProviders = [
    {
        provide: 'CronPostsModel',
        useFactory: (connection) => connection.model('CronPosts', cronPosts_schema_1.CronPostsSchema),
        inject: ['DATABASE_CONNECTION'],
    },
];
//# sourceMappingURL=cronPosts.providers.js.map