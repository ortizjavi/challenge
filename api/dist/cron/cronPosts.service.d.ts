import { Model } from 'mongoose';
import { CronPosts } from './interfaces/cronPosts.interface';
import { PostService } from '../post/post.service';
import { ExternalPostsService } from '../services/externalPosts/externalPosts.service';
export declare class CronPostsService {
    private readonly cronPostsModel;
    private readonly postService;
    private readonly extPostsService;
    private readonly logger;
    private lastUpdatedPost;
    constructor(cronPostsModel: Model<CronPosts>, postService: PostService, extPostsService: ExternalPostsService);
    private initializePosts;
    private saveLastPosts;
    runEveryHour(): void;
    private saveLastDate;
}
