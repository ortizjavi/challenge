"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CronPostsSchema = void 0;
const mongoose = require("mongoose");
exports.CronPostsSchema = new mongoose.Schema({
    last_updated: {
        type: Date,
        allowNull: false,
        'default': null
    }
}, {
    capped: {
        size: 1024,
        max: 1,
    },
    timestamps: false
});
//# sourceMappingURL=cronPosts.schema.js.map