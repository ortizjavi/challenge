"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CronSchema = void 0;
const mongoose = require("mongoose");
exports.CronSchema = new mongoose.Schema({
    last_updated: {
        type: Date,
        allowNull: false,
        'default': null
    }
}, { timestamps: false });
//# sourceMappingURL=cron.schema.js.map