import { Model } from 'mongoose';
import { CronPosts } from './interfaces/cronPosts.interface';
import { PostService } from '../post/post.service';
export declare class CronService {
    private readonly cronPostsModel;
    private readonly postService;
    private readonly logger;
    private lastUpdatePosts;
    constructor(cronPostsModel: Model<CronPosts>, postService: PostService);
    runEveryHour(): void;
}
