import { Document } from 'mongoose';
export interface Cron extends Document {
    readonly last_updated: Date;
}
