"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.cronProviders = void 0;
const cronPosts_schema_1 = require("./schemas/cronPosts.schema");
exports.cronProviders = [
    {
        provide: 'CronModel',
        useFactory: (connection) => connection.model('CronPosts', cronPosts_schema_1.CronPostsSchema),
        inject: ['DATABASE_CONNECTION'],
    },
];
//# sourceMappingURL=cron.providers.js.map