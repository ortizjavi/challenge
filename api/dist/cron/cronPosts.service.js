"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var CronPostsService_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.CronPostsService = void 0;
const mongoose_1 = require("mongoose");
const common_1 = require("@nestjs/common");
const schedule_1 = require("@nestjs/schedule");
const post_service_1 = require("../post/post.service");
const externalPosts_service_1 = require("../services/externalPosts/externalPosts.service");
let CronPostsService = CronPostsService_1 = class CronPostsService {
    constructor(cronPostsModel, postService, extPostsService) {
        this.cronPostsModel = cronPostsModel;
        this.postService = postService;
        this.extPostsService = extPostsService;
        this.logger = new common_1.Logger(CronPostsService_1.name);
        this.lastUpdatedPost = null;
        this.lastUpdatedPost = this.cronPostsModel.findOne({}, (err, lastUpdated) => {
            console.log(lastUpdated);
            this.lastUpdatedPost = lastUpdated;
            if (!lastUpdated) {
                this.initializePosts();
            }
        });
    }
    initializePosts() {
        this.extPostsService.getPosts().then(response => {
            const apiPosts = response.data.hits;
            let posts = [];
            for (let post of apiPosts) {
                post.url = post.url || post.story_url;
                post.title = post.title || post.story_title;
                if (post.url && post.title) {
                    posts.push(post);
                }
            }
            this.postService.bulkCreate(posts);
            this.saveLastDate(apiPosts.shift());
        });
    }
    saveLastPosts() {
        this.extPostsService.getPosts().then(response => {
            const apiPosts = response.data.hits;
            let posts = [];
            for (let post of apiPosts) {
                if (post.created_at > this.lastUpdatedPost) {
                    post.url = post.url || post.story_url;
                    post.title = post.title || post.story_title;
                    if (post.url && post.title) {
                        posts.push(post);
                    }
                }
            }
            if (posts.length) {
                this.postService.bulkCreate(posts);
            }
            this.saveLastDate(apiPosts.shift());
        });
    }
    runEveryHour() {
        this.logger.debug(`Called at ${new Date()}`);
        this.saveLastPosts();
    }
    async saveLastDate(post) {
        const date = post.created_at;
        console.log(date);
        this.lastUpdatedPost = date;
        const newDate = await new this.cronPostsModel({ last_updated: date });
        return newDate.save();
    }
};
__decorate([
    (0, schedule_1.Cron)(schedule_1.CronExpression.EVERY_HOUR),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], CronPostsService.prototype, "runEveryHour", null);
CronPostsService = CronPostsService_1 = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)('CronPostsModel')),
    __metadata("design:paramtypes", [mongoose_1.Model,
        post_service_1.PostService,
        externalPosts_service_1.ExternalPostsService])
], CronPostsService);
exports.CronPostsService = CronPostsService;
//# sourceMappingURL=cronPosts.service.js.map