import { HttpService } from '@nestjs/axios';
import { AxiosResponse } from 'axios';
export declare class ExternalPostsService {
    private readonly httpService;
    private readonly url;
    constructor(httpService: HttpService);
    getPosts(): Promise<AxiosResponse>;
}
