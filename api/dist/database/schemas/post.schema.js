"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CatSchema = void 0;
const mongoose = require("mongoose");
exports.CatSchema = new mongoose.Schema({
    title: String,
    url: String,
    created_at_i: String,
    author: String
}, { timestamps: false });
//# sourceMappingURL=post.schema.js.map