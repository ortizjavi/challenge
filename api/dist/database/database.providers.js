"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.databaseProviders = void 0;
const mongoose = require("mongoose");
const environments_1 = require("../environments/environments");
exports.databaseProviders = [
    {
        provide: 'DATABASE_CONNECTION',
        useFactory: async () => {
            const connection = await mongoose.connect(process.env.DB_URL || environments_1.env.mongodb.uri, {
                useNewUrlParser: true,
                useUnifiedTopology: true
            });
            if (environments_1.env.mongodb.reset)
                mongoose.connection.db.dropDatabase();
            return connection;
        },
    },
];
//# sourceMappingURL=database.providers.js.map