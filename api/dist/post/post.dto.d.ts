export declare class PostDto {
    readonly title: string;
    readonly url: string;
    readonly created_at: Date;
    readonly author: string;
}
