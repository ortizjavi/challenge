import { PostService } from './post.service';
export declare class PostController {
    private postService;
    constructor(postService: PostService);
    getPosts(): Promise<import("./interfaces/post.interface").Post[]>;
    deletePost(id: number): Promise<import("./interfaces/post.interface").Post>;
}
