import { Model } from 'mongoose';
import { Post } from './interfaces/post.interface';
import { PostDto } from './post.dto';
export declare class PostService {
    private readonly postModel;
    constructor(postModel: Model<Post>);
    bulkCreate(postsDto: PostDto[]): Promise<Post[]>;
    findAll(): Promise<Post[]>;
    deleteOne(id: number): Promise<Post>;
}
