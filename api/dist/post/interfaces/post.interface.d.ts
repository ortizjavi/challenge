import { Document } from 'mongoose';
export interface Post extends Document {
    readonly title: string;
    readonly url: string;
    readonly created_at: Date;
    readonly author: string;
}
