import { Test, TestingModule } from '@nestjs/testing';
import { PostController } from './post.controller';
import { PostService } from './post.service';
import { postProviders } from './post.providers';
import { DatabaseModule } from '../database/database.module';
import { PostSchema } from './schemas/post.schema';

const providers = [ PostService, ...postProviders ]

describe('PostController', () => {
  let controller: PostController;

  beforeEach(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [ DatabaseModule ],
      controllers: [PostController],
    }).compile();

    controller = moduleRef.resolve<PostController>(PostController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
