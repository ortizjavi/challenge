import { Module } from '@nestjs/common';
import { PostController } from './post.controller';
import { PostService } from './post.service';
import { postProviders } from './post.providers';
import { DatabaseModule } from '../database/database.module';
import { PostSchema } from './schemas/post.schema';

const providers = [ PostService, ...postProviders ]
@Module({
  imports: [ DatabaseModule ],
  controllers: [PostController],
  providers,
  exports: [ PostService ]
})
export class PostModule {}
