import { Body, Param, Post, Get, Delete, Controller } from '@nestjs/common';
import { HttpException, HttpStatus } from '@nestjs/common';
import { PostService } from './post.service';
import { PostDto } from './post.dto';

@Controller('post')
export class PostController {
	constructor(private postService: PostService){}

	@Get()
	public getPosts(){
		return this.postService.findAll();
	}

	@Delete(':id')
	public deletePost(@Param('id') id: number){
		return this.postService.deleteOne(id)
		.then(deleted => {
			if (!deleted){
			  throw new HttpException('Not found', HttpStatus.NOT_FOUND);
			}
			return deleted;
		});
	}
}
