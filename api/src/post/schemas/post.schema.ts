import * as mongoose from 'mongoose';

export const PostSchema = new mongoose.Schema({
  title: {
  	type: String,
  	required: true
  },
  url: {
  	type: String,
  	required: true
  },
  created_at: Date,
  author: String
}, { timestamps : false });