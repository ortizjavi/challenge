import { Model } from 'mongoose';
import { Injectable, Inject, HttpException, HttpStatus } from '@nestjs/common';
import { Post } from './interfaces/post.interface';
import { PostDto } from './post.dto';

@Injectable()
export class PostService {
  constructor(
    @Inject('PostModel')
    private readonly postModel: Model<Post>,
  ) {}

  async bulkCreate(postsDto: PostDto[]): Promise<Post[]> {
      return this.postModel.insertMany(postsDto);
  }

  async findAll(): Promise<Post[]> {
    return this.postModel.find().exec();
  }

  async deleteOne(id : number): Promise<Post> {
    return this.postModel.findOneAndDelete({ _id: id });
  }
}
