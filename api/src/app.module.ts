import { Module } from '@nestjs/common';

import { PostModule } from './post/post.module';
import { ScheduleModule } from '@nestjs/schedule';
import { CronPostsModule } from './cron/cronPosts.module';

@Module({
  imports: [
  	PostModule,
  	ScheduleModule.forRoot(),
  	CronPostsModule,
  ],
})
export class AppModule {}
