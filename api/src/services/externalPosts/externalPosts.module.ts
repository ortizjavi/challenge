import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { ExternalPostsService } from './externalPosts.service';

@Module({
	imports: [ HttpModule ],
	providers: [ExternalPostsService],
	exports: [ExternalPostsService]
})
export class ExternalPostsModule {}
