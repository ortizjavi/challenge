import { Test, TestingModule } from '@nestjs/testing';
import { ExternalPostsService } from './externalPosts.service';
import { HttpModule } from '@nestjs/axios';

describe('PostsServiceService', () => {
  let service: ExternalPostsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [ExternalPostsService],
    }).compile();

    service = module.get<ExternalPostsService>(ExternalPostsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
