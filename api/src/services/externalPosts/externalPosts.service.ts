import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { AxiosResponse } from 'axios'

@Injectable()
export class ExternalPostsService {
	private readonly url = "https://hn.algolia.com/api/v1/search_by_date?query=nodejs";

	constructor(private readonly httpService: HttpService) {}
	getPosts() : Promise<AxiosResponse> {
		return this.httpService.get(this.url).toPromise();
	}
}
