import * as mongoose from 'mongoose';
import { env } from '../environments/environments';

export const databaseProviders = [
  {
    provide: 'DATABASE_CONNECTION',
    useFactory: async (): Promise<typeof mongoose> =>{
        const connection = await mongoose.connect(
        	process.env.DB_URL || env.mongodb.uri,
        	{
        		useNewUrlParser: true,
        		useUnifiedTopology: true
        	}
        );
        if (env.mongodb.reset)
	        mongoose.connection.db.dropDatabase();
        return connection;
    },
  },
];
