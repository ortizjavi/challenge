import { Model } from 'mongoose';
import { Inject, Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { CronPosts } from './interfaces/cronPosts.interface';
import { PostService } from '../post/post.service';
import { PostDto } from '../post/post.dto';
import { ExternalPostsService } from '../services/externalPosts/externalPosts.service';


@Injectable()
export class CronPostsService {
  private readonly logger = new Logger(CronPostsService.name);
  private lastUpdatedPost = null;
  constructor(
  	@Inject('CronPostsModel')
    private readonly cronPostsModel: Model<CronPosts>,
  	private readonly postService: PostService,
  	private readonly extPostsService : ExternalPostsService
  ){
  	this.lastUpdatedPost = this.cronPostsModel.findOne(
  		{}, 
  		(err, lastUpdated) => {
  			console.log(lastUpdated);
			this.lastUpdatedPost = lastUpdated;
			if (!lastUpdated){
				this.initializePosts();
			}
  		}
  	);
  }

  private initializePosts(){
  	this.extPostsService.getPosts().then(response => {
  		const apiPosts = response.data.hits;
  		let posts = [];
  		for (let post of apiPosts){
  			post.url = post.url || post.story_url;
  			post.title = post.title || post.story_title;
  			if (post.url && post.title){
  				posts.push(post);
  			}
  		}
		this.postService.bulkCreate(<PostDto []> posts);

		this.saveLastDate(apiPosts.shift());
	})
  }

  private saveLastPosts(){
  	this.extPostsService.getPosts().then(response => {
  		const apiPosts = response.data.hits;
  		let posts = [];
  		for (let post of apiPosts){
  			if (post.created_at > this.lastUpdatedPost){
  				post.url = post.url || post.story_url;
	  			post.title = post.title || post.story_title;
	  			if (post.url && post.title){
	  				posts.push(post);
	  			}
  			}
  		}
  		if (posts.length){
  			this.postService.bulkCreate(<PostDto []> posts);
  		}
  		this.saveLastDate(apiPosts.shift());
    })
  }

  @Cron(CronExpression.EVERY_HOUR)
  runEveryHour() {
    this.logger.debug(`Called at ${new Date()}`);
    this.saveLastPosts();
  }


  private async saveLastDate(post: PostDto){
  	const date = post.created_at;
  	console.log(date);
  	this.lastUpdatedPost = date;
  	const newDate = await new this.cronPostsModel({ last_updated : date });
  	return newDate.save();
  }
}
