import { Test, TestingModule } from '@nestjs/testing';
import { CronPostsService } from './cronPosts.service';
import { cronPostsProviders } from './cronPosts.providers';
import { PostModule } from '../post/post.module';
import { DatabaseModule } from '../database/database.module';
import { ExternalPostsModule } from '../services/externalPosts/externalPosts.module';

describe('CronPostsService', () => {
  let service: CronPostsService;

  beforeEach(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [ 
        PostModule,
        ExternalPostsModule,
        DatabaseModule
      ],
      providers: [CronPostsService, ...cronPostsProviders],
    }).compile();

    service = moduleRef.get<CronPostsService>(CronPostsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
