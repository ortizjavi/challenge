import { Connection } from 'mongoose';
import { CronPostsSchema } from './schemas/cronPosts.schema';

export const cronPostsProviders = [
  {
    provide: 'CronPostsModel',
    useFactory: (connection: Connection) => connection.model('CronPosts', CronPostsSchema),
    inject: ['DATABASE_CONNECTION'],
  },
];
