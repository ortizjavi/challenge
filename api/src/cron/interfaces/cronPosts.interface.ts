import { Document } from 'mongoose';

export interface CronPosts extends Document {
  readonly last_updated: Date;
}
