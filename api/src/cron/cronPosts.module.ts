import { Module } from '@nestjs/common';
import { PostModule } from '../post/post.module';
import { DatabaseModule } from '../database/database.module';
import { CronPostsService } from './cronPosts.service';
import { cronPostsProviders } from './cronPosts.providers';
import { ExternalPostsModule } from '../services/externalPosts/externalPosts.module';

@Module({
  imports: [ 
  	PostModule,
  	ExternalPostsModule,
  	DatabaseModule
  ],
  providers: [
  	CronPostsService,
  	...cronPostsProviders
  ]
})
export class CronPostsModule {}
