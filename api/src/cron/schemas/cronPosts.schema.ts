import * as mongoose from 'mongoose';

export const CronPostsSchema = new mongoose.Schema({
  last_updated: {
    type: Date,
    allowNull: false,
    'default': null
  }
}, { 
  capped : {
    size: 1024,
    max: 1,
  },
  timestamps : false 
});
