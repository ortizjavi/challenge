# Challenge

NestJS backend challenge

## Prerequisites
- Node (>= 14)
- Docker

## Run Locally
```
cd api && npm run start:local
```

## Run dockerized
```
cd api && docker-compose up
```


### You can force a database reset on api/src/enviroments.ts
